/*
 * Application.c
 *
 *  Created on: Jan 16, 2024
 *      Author: Krzysztof Moskwa
 *      License: GPL-3.0-or-later
 *
 *  kmTimer5Test Application for testing kmTimer5 library from AVR kmFramework
 *  Copyright (C) 2024  Krzysztof Moskwa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"


#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer5/kmTimer5.h"

#define KM_TIMER5_TEST_1MS 1000 // number of microseconds defining 1 millisecond
#define KM_TIMER5_TEST_5MS 5000 // number of microseconds defining 5 millisecond
#define KM_TIMER5_TEST_USER_DATA_A 1UL
#define KM_TIMER5_TEST_USER_DATA_B 65535UL
#define KM_TIMER5_TEST_USER_DATA_C 255UL
#define KM_TIMER5_TEST_USER_DATA_D 4096UL
#define KM_TIMER5_TEST_DUTY_0_PERC KM_TIMER5_BOTTOM
#define KM_TIMER5_TEST_DUTY_25_PERC KM_TIMER5_MID - (KM_TIMER5_MID >> KMC_DIV_BY_2)
#define KM_TIMER5_TEST_DUTY_50_PERC KM_TIMER5_MID
#define KM_TIMER5_TEST_DUTY_75_PERC KM_TIMER5_MID + (KM_TIMER5_MID >> KMC_DIV_BY_2)
#define KM_TIMER5_TEST_DUTY_100_PERC KM_TIMER5_MAX

#define KM_TIMER5_TEST_PHASE_45_DEG KM_TIMER5_TEST_DUTY_25_PERC
#define KM_TIMER5_TEST_PHASE_90_DEG KM_TIMER5_TEST_DUTY_50_PERC
#define KM_TIMER5_TEST_PHASE_135_DEG KM_TIMER5_TEST_DUTY_75_PERC

// "private" variables
static const uint16_t swipeTestValues[] = { 0x0000, 0x0100, 0x1000, 0x2000, 0x4000, 0x8000, 0xc000, 0xFA00, 0xFF00, 0xFE00, 0x80 };

// "private" functions - declarations
uint16_t getSwipeTestRange(void);
void callbackOVF(void *userData);
void callbackOVFToggle(void *userData);
void callbackCompAToggle(void *userData);
void callbackCompBToggle(void *userData);
void callbackCompCToggle(void *userData);
void callbackCompAOff(void *userData);
void callbackCompBOff(void *userData);
void callbackCompCOff(void *userData);
void testSwipeCompATable(void);
void testSwipeCompA(void);
void testSwipeCompBTable(void);
void testSwipeCompB(void);
void testSwipeCompCTable(void);
void testSwipeCompC(void);
void testSwipePwm(const Tcc5PwmOut pwmOut, uint16_t maxValue);
void testSwipePwmTable(const Tcc5PwmOut pwmOut);
void appTest0(void);
void appTest1(void);
void appTest2(void);
void appTest3(void);
void appTest4(void);
void appTest5(void);
void appTest6(void);
void appTest7(void);
void appTest8(void);
void appInitDebug(void);

// "private" functions - definitions
uint16_t getSwipeTestRange(void) {
	return sizeof(swipeTestValues) / sizeof(swipeTestValues[0]);
}

void callbackOVF(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
	dbOn(DB_PIN_1);
	dbOn(DB_PIN_2);
	dbOn(DB_PIN_3);
}

void callbackOVFToggle(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
}

void callbackCompAToggle(void *userData) {
	//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_1);
}

void callbackCompBToggle(void *userData) {
//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_2);
}

void callbackCompCToggle(void *userData) {
//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_3);
}

void callbackCompAOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_2);
}

void callbackCompCOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_3);
}

void testSwipeCompATable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetValueCompA(swipeTestValues[i]);
	}
}

void testSwipeCompA(void) {
	for (uint16_t i = 0; i < KM_TIMER5_MAX - KM_TIMER5_TEST_SWIPE_ACCURACY; i += KM_TIMER5_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetValueCompA(i);
	}
}

void testSwipeCompBTable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (uint16_t i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetValueCompB(swipeTestValues[i]);
	}
}

void testSwipeCompB(void) {
	for (uint16_t i = 0; i < KM_TIMER5_MAX - KM_TIMER5_TEST_SWIPE_ACCURACY; i += KM_TIMER5_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetValueCompB(i);
	}
}

void testSwipeCompCTable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (uint16_t i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetValueCompC(swipeTestValues[i]);
	}
}

void testSwipeCompC(void) {
	for (uint16_t i = 0; i < KM_TIMER5_MAX - KM_TIMER5_TEST_SWIPE_ACCURACY; i += KM_TIMER5_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetValueCompC(i);
	}
}

void testSwipePwm(const Tcc5PwmOut pwmOut, uint16_t maxValue) {
	kmTimer5SetPwmInversion(pwmOut, false);
	for (uint16_t i = 0; i <= maxValue - KM_TIMER5_TEST_SWIPE_ACCURACY; i+=KM_TIMER5_TEST_SWIPE_ACCURACY) {
		kmTimer5SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER5_TEST_SWIPE_DELAY);
	}

	kmTimer5SetPwmInversion(pwmOut, true);
	for (uint16_t i = maxValue; i >= KM_TIMER5_TEST_SWIPE_ACCURACY; i-=KM_TIMER5_TEST_SWIPE_ACCURACY) {
		kmTimer5SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER5_TEST_SWIPE_DELAY);
	}
}

void testSwipePwmTable(const Tcc5PwmOut pwmOut) {
	uint16_t swipeRange = getSwipeTestRange();

	kmTimer5SetPwmInversion(pwmOut, false);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
	for (int i = 0; i < 10; i++) {
		_delay_ms(10);
		dbToggle(DB_PIN_4);
	}
	kmTimer5SetPwmInversion(pwmOut, true);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER5_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer5SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
}

void appTest0(void) {
	kmTimer5InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER5_TEST_1MS);
	kmTimer5Start();
}

void appTest1(void) {
	kmTimer5InitOnAccurateTimeCompAInterruptCallback(KM_TIMER5_TEST_5MS, true);

#ifdef KM_TIMER5_TEST_INTERRUPTS
	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();
#endif /* KM_TIMER5_TEST_INTERRUPTS */

	kmTimer5Start();
}

void appTest2(void) {
	kmTimer5InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(KM_TCC0_PRSC_8);

#ifdef KM_TIMER5_TEST_INTERRUPTS
	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer5EnableInterruptOVF();

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5SetValueCompA(KM_TIMER5_TEST_DUTY_25_PERC);
	kmTimer5EnableInterruptCompA();

#ifdef OCR5B
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5SetValueCompB(KM_TIMER5_TEST_DUTY_50_PERC);
	kmTimer5EnableInterruptCompB();
#endif /* OCR5B */
#ifdef OCR5C
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer5SetValueCompC(KM_TIMER5_TEST_DUTY_75_PERC);
	kmTimer5EnableInterruptCompC();
#endif /* OCR5C */
#endif /* KM_TIMER5_TEST_INTERRUPTS */

	kmTimer5ConfigureOCA(KM_TCC5_A_COMP_OUT_TOGGLE);
#ifdef OCR5B
	kmTimer5ConfigureOCB(KM_TCC5_B_COMP_OUT_TOGGLE);
#endif /* OCR5B */
#ifdef OCR5C
	kmTimer5ConfigureOCC(KM_TCC5_C_COMP_OUT_TOGGLE);
#endif /* OCR5C */

	kmTimer5Start();

#ifdef KM_TIMER5_TEST_SWIPE
	testSwipeCompA();
	kmTimer5SetValueCompA(KM_TIMER5_MID);
#ifdef OCR5B
	testSwipeCompB();
	kmTimer5SetValueCompB(KM_TIMER5_MID);
#endif /* OCR5B */
#ifdef OCR5C
	testSwipeCompC();
	kmTimer5SetValueCompC(KM_TIMER5_MID);
#endif /* OCR5C */
#endif /* KM_TIMER5_TEST_SWIPE */

}

void appTest3(void) {
	kmTimer5InitOnAccurateTimeCompABInterruptCallback(KM_TIMER5_TEST_1MS, KM_TIMER5_TEST_PHASE_45_DEG); // for 1 millisecond

#ifdef KM_TIMER5_TEST_INTERRUPTS
	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();

#ifdef OCR5B
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5EnableInterruptCompB();
#endif /* OCR5B */

#endif /* KM_TIMER5_TEST_INTERRUPTS */
	kmTimer5Start();
}

void appTest4(void) {
	kmTimer5InitOnPrescalerBottomToTopFastPwm(KM_TCC5_PRSC_1, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_75_PERC, false);

#ifdef KM_TIMER5_TEST_INTERRUPTS
	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer5EnableInterruptOVF();

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();
#ifdef OCR5B
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5EnableInterruptCompB();
#endif /* OCR5B */

#ifdef OCR5C
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer5EnableInterruptCompC();
#endif /* OCR5C */
#endif /* KM_TIMER5_TEST_INTERRUPTS */

	kmTimer5Start();

#ifdef KM_TIMER5_TEST_SWIPE
#ifdef OCR5B
	testSwipePwm(KM_TCC5_PWM_OUT_B, KM_TIMER5_MAX);
#endif /* OCR5B */
	testSwipePwm(KM_TCC5_PWM_OUT_A, KM_TIMER5_MAX);
#endif /* KM_TIMER5_TEST_SWIPE */
}

void appTest5(void) {
	uint16_t cyclesRange = kmTimer5InitOnAccurateTimeFastPwm(KM_TIMER5_TEST_1MS, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_25_PERC, false);

#ifdef KM_TIMER5_TEST_INTERRUPTS
	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer5EnableInterruptOVF();

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();

#ifdef OCR5B
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5EnableInterruptCompB();
#endif /* OCR5B */

#ifdef OCR5C
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer5EnableInterruptCompC();
#endif /* OCR5C */

#endif /* KM_TIMER5_TEST_INTERRUPTS */
	kmTimer5Start();

#ifdef KM_TIMER5_TEST_SWIPE
#ifdef OCR5C
	testSwipePwm(KM_TCC5_PWM_OUT_C, cyclesRange);
	kmTimer5SetValueCompC(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR5C */
#ifdef OCR5B
	testSwipePwm(KM_TCC5_PWM_OUT_B, cyclesRange);
	kmTimer5SetValueCompB(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR5B */
	testSwipePwm(KM_TCC5_PWM_OUT_A, cyclesRange);
	kmTimer5SetValueCompA(cyclesRange >> KMC_DIV_BY_2);
#endif /* KM_TIMER5_TEST_SWIPE */
}

void appTest6(void) {
	kmTimer5InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_1, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_75_PERC, false);

#ifdef KM_TIMER5_TEST_INTERRUPTS
	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVF);
	kmTimer5EnableInterruptOVF();

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer5EnableInterruptCompA();
#ifdef OCR5B
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer5EnableInterruptCompB();
#endif /* OCR5B */

#ifdef OCR5C
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer5EnableInterruptCompC();
#endif /* OCR5C */
#endif /* KM_TIMER5_TEST_INTERRUPTS */
	kmTimer5Start();

#ifdef KM_TIMER5_TEST_SWIPE
	testSwipePwm(KM_TCC5_PWM_OUT_A, KM_TIMER5_MAX);
	kmTimer5SetValueCompA(KM_TIMER5_MID);
#ifdef OCR5B
	testSwipePwm(KM_TCC5_PWM_OUT_B, KM_TIMER5_MAX);
	kmTimer5SetValueCompB(KM_TIMER5_MID);
#endif /* OCR5B */
#ifdef OCR5C
	testSwipePwm(KM_TCC5_PWM_OUT_C, KM_TIMER5_MAX);
	kmTimer5SetValueCompC(KM_TIMER5_MID);
#endif /* OCR5C */
#endif /* KM_TIMER5_TEST_SWIPE */
}

void appTest7(void) {
	uint16_t cyclesRange = kmTimer5InitOnAccurateTimePcPwm(KM_TIMER5_TEST_1MS, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_75_PERC, false);

#ifdef KM_TIMER5_TEST_INTERRUPTS
	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVF);
	kmTimer5EnableInterruptOVF();

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer5EnableInterruptCompA();
#ifdef OCR5B
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer5EnableInterruptCompB();
#endif /* OCR5B */

#ifdef OCR5C
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer5EnableInterruptCompC();
#endif /* OCR5C */
#endif /* KM_TIMER5_TEST_INTERRUPTS */

	kmTimer5Start();

#ifdef KM_TIMER5_TEST_SWIPE
	testSwipePwm(KM_TCC5_PWM_OUT_A, cyclesRange);
	kmTimer5SetValueCompA(cyclesRange >> KMC_DIV_BY_2);
#ifdef OCR5B
	testSwipePwm(KM_TCC5_PWM_OUT_B, cyclesRange);
	kmTimer5SetValueCompB(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR5B */
#ifdef OCR5C
	testSwipePwm(KM_TCC5_PWM_OUT_C, cyclesRange);
	kmTimer5SetValueCompC(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR5C */
#endif /* KM_TIMER5_TEST_SWIPE */
}

void appTest8(void) {
	kmTimer5Init(KM_TCC5_PRSC_1024, KM_TCC5_MODE_7_A, KM_TCC5_MODE_7_B);

	kmTimer5SetValueCompA(4);
	kmTimer5ConfigureOCA(KM_TCC5_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);

#ifdef OCR5B
	kmTimer5SetValueCompB(9);
	kmTimer5ConfigureOCB(KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
#endif /* OCR5B */
#ifdef OCR5C
	kmTimer5SetValueCompC(128);
	kmTimer5ConfigureOCC(KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
#endif /* OCR5C */

	kmTimer5Start();
}

void appInitDebug(void) {
	dbPullUpAllPorts();
	dbInit();
	dbOff(DB_PIN_0);
	dbOff(DB_PIN_1);
	dbOff(DB_PIN_2);
	dbOff(DB_PIN_3);
	dbOff(DB_PIN_4);
}

// "public" functions
void appInit(void) {
	kmCpuDisableWatchdogAndInterruptsOnStartup();
	appInitDebug();
	kmCpuInterruptsEnable();

	static const uint16_t testNumber = KM_TIMER5_TEST_NUMBER;

	switch (testNumber) {
		case 0: {
			appTest0();
			break;
		}
		case 1: {
			appTest1();
			break;
		}
		case 2: {
			appTest2();
			break;
		}
		case 3: {
			appTest3();
			break;
		}
		case 4: {
			appTest4();
			break;
		}
		case 5: {
			appTest5();
			break;
		}
		case 6: {
			appTest6();
			break;
		}
		case 7: {
			appTest7();
			break;
		}
		case 8: {
			appTest8();
			break;
		}
	}
}

void appLoop(void) {
}
